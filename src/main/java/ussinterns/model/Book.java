package ussinterns.model;

/**
 * Represents a Book.
 *
 * @author John Fresco
 */
public class Book {
    private String id;
    private String title;
    private String author;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }
}
