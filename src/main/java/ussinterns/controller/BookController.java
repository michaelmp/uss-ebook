package ussinterns.controller;


import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.ServletContextResource;
import ussinterns.model.Book;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Handles stuff like loading books and stuff.
 *
 * @author Victor Avila, John Fresco
 */
@Controller
public class BookController {

    /* Needed to find the relative path */
    private @Autowired
    ServletContext servletContext;

    /**
     * TODO: Returns a list of available books.
     *
     * @return
     */
	@RequestMapping(value="/books", method=RequestMethod.GET)
	public @ResponseBody Map bookList() {
		Map<String, String> model = new HashMap<String, String>();
		model.put("Hello", "World!?");
		return model;
	}

    /**
     * Returns a JSON with details of the requested book
     *
     * @param id
     * @return
     */
    @RequestMapping(value="/books/{id}", method=RequestMethod.GET)
	public @ResponseBody Book books(@PathVariable(value="id") String id) {
        return findBook(id);
	}

    /**
     * Returns a Book with the requested Id
     *
     * @param id
     * @return
     */
    private Book findBook(String id) {
        try {

            /*
             * We're using a ServletContextResource to find the file with a path
             * relative to the servlet root
             */
            ServletContextResource resource =
                    new ServletContextResource(servletContext, "./resources/manifest.json");
            File file = resource.getFile();

            /* Does magical work of converting JSON to objects. */
            ObjectMapper mapper = new ObjectMapper();

            /*
             * Once we have the root object, we get the array under the index
             * key. Then we iterate over all the different objects checking if
             * the object has the requested id. If we find, we map it to a Book
             * object which we return to the previous method.
             */
            JsonNode root = mapper.readValue(file, JsonNode.class);
            JsonNode index = root.path("index");

            Iterator<JsonNode> booksArray = index.getElements();

            while (booksArray.hasNext()) {
                JsonNode bookObject = booksArray.next();

                if (bookObject.path("id").getTextValue().equals(id)) {
                    Book book = mapper.readValue(bookObject, Book.class);
                    return book;
                }
            }
        } catch (IOException e) {
            System.out.println("ZOMG: IO ERROR");
            e.printStackTrace();
        }

        return null;
    }
}