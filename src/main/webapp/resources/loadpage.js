/**
 * loadpage
 * JS logic to load and scroll the pages
 * Author: victor avila, john fresco
 * 
 */
(function() {
	'use strict';
	
	this.BookLoader = {
		loadBook: function loadBook(id) {
			$('#textL').load('./resources/books/' + id + '.html', function() {
				$('#textL').ready(function() {
					$('#textL').scrollTop();
				});
			});
			$('#textR').load('./resources/books/' + id + '.html', function() {
				$('#textR').ready(function() {
					$('#textR').scrollTop(640);
				});
			});
		},
		initScroll: (function initScroll() {
			// Scroll increment for full-screen viewing
			var sc = 1280;
			if ($(window).width() < 1375 || $(window).height() < 650) {
				sc = 480;
			}

			$(window).resize(function() {
				if ($(".page:hidden").length === 1) {
					//Triggered when the window resizes and hides the right text 
					//area.
					sc = 480;
					$('#textL').scrollTop(Math.ceil(($('#textL').scrollTop())));
				}
				else {
					sc = 1280;
					$('#textL').scrollTop(Math.ceil(($('#textL').scrollTop())));
					$('#textR').scrollTop(Math.ceil(($('#textL').scrollTop())) + 640);
				}
			});
			$('#right').click(function() {
				$('#textL').scrollTop(Math.ceil(($('#textL').scrollTop())) + sc);
				$('#textR').scrollTop(Math.ceil(($('#textL').scrollTop())) + 640);
			});
			$('#left').click(function() {
				$('#textL').scrollTop(Math.ceil(($('#textL').scrollTop())) - sc);
				$('#textR').scrollTop(Math.ceil(($('#textL').scrollTop())) + 640);
				if ($('#textR').scrollTop() === 0) {
					$('#textR').scrollTop(640);
				}
			});
		})
	};
}).call(this);

document.addEventListener("DOMContentLoaded", function(){
	BookLoader.initScroll();
});