<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<title>Welcome!</title>
	<body>
		<!--
			Beginings of a welcome page with a search feature 
		-->
		<h1>Welcome to your eBook, please choose a book:</h1>
		<a rel="alternate" href="index.html">Dracula</a>
		<input type="text" name="${status.expression}" value="${status.value}">
		<input type="submit" value="Search"/>
	</body>
</html>