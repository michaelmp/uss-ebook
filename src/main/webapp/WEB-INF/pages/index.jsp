<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
        <title>eReader</title>
        <script src="resources/jquery-2.0.3.min.js"></script>

		<link rel="stylesheet" type="text/css" href="resources/reader.css"/>
        <script type="text/javascript" src="resources/loadpage.js"></script>
	</head>
	<body>
		<div class="text">
			<div id="textL" class="page"></div>
			<div id="textR" class="page"></div>
		</div>
		<div class="button">
			<div id="left"></div>
			<div id="right"></div>
		</div>
		<select onchange="BookLoader.loadBook(this.value)">
			<option value="354">Dracula</option>	
			<option value="2199">Illiad</option>
			<option value="1162">The Jacket (Star-Rover)</option>
		</select>
	</body>
</html>